<?php

declare(strict_types=1);

namespace App\Tests\Unit\Car;

use App\Car\Car;
use App\Car\CarFactory;
use App\Car\CarType;
use App\Car\Dto;
use PHPUnit\Framework\TestCase;

class CarFactoryTest extends TestCase
{
    public function test_success_create(): void
    {
        $dto = new Dto(
            'car',
            'Nissan',
            'photo.jpg',
            '1.2',
            '9',
            '',
            '',
        );

        $factory = new CarFactory();
        /** @var Car $car */
        $car = $factory->create($dto);

        $this->assertEquals(CarType::CAR, $car->getCarType());
        $this->assertEquals($dto->brand, $car->getBrand());
        $this->assertEquals($dto->photoFileName, $car->getPhotoFileName());
        $this->assertEquals((float)$dto->carrying, $car->getCarrying());
        $this->assertEquals((int)$dto->passengerSeatsCount, $car->getPassengerSeatsCount());
    }

    public function test_unexpected_car_type(): void
    {
        $dto = new Dto(
            'Nissan car',
            'Nissan',
            'photo.jpg',
            '1.2',
            '9',
            '',
            '',
        );

        $this->expectExceptionMessage('Unexpected match value');

        $factory = new CarFactory();
        $factory->create($dto);
    }
}
