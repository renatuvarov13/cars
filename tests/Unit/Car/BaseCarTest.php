<?php

declare(strict_types=1);

namespace App\Tests\Unit\Car;

use App\Car\Car;
use PHPUnit\Framework\TestCase;

class BaseCarTest extends TestCase
{
    public function test_get_extension(): void
    {
        $car = new Car();
        $car->setPhotoFileName('file.jpg');

        $this->assertEquals('.jpg', $car->getPhotoFileExt());
    }

    public function test_get_empty_extension(): void
    {
        $car = new Car();
        $car->setPhotoFileName('file');

        $this->assertNull($car->getPhotoFileExt());
    }
}
