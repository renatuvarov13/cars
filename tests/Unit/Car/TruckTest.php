<?php

declare(strict_types=1);

namespace App\Tests\Unit\Car;

use App\Car\Truck;
use PHPUnit\Framework\TestCase;

class TruckTest extends TestCase
{
    public function test_body_volume(): void
    {
        $car = new Truck();
        $car->setBodyWidth(3.0);
        $car->setBodyHeight(3.0);
        $car->setBodyLength(3.0);

        $this->assertEquals(27.0, $car->getBodyVolume());
    }
}
