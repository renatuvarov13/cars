<?php

declare(strict_types=1);

namespace App\Tests\Functional\Controller;

use App\Car\BaseCar;
use App\Handler\CarListHandler;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ControllerTest extends WebTestCase
{
    public function test_index(): void
    {
        $client = static::createClient();

        $container = self::getContainer();
        $getCarList = $container->get(CarListHandler::class);
        /** @var BaseCar[] $carList */
        $carList = $getCarList(__DIR__ . '/../../assets/Исходные данные для задания с машинами.csv');

        $client->request(
            method: 'POST',
            uri: '/',
            files: [
                'cars' => new UploadedFile(
                    __DIR__ . '/../../assets/Исходные данные для задания с машинами.csv',
                    'Исходные данные для задания с машинами.csv',
                )
            ]
        );
        $this->assertResponseIsSuccessful();

        $content = json_decode($client->getResponse()->getContent(), true);
        foreach ($carList as $i => $car) {
            $this->assertEquals($car->getCarType()->value, $content[$i]['carType']);
            $this->assertEquals($car->getBrand(), $content[$i]['brand']);
            $this->assertEquals($car->getCarrying(), $content[$i]['carrying']);
            $this->assertEquals($car->getPhotoFileName(), $content[$i]['photoFileName']);
        }
    }
}
