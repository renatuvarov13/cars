<?php

declare(strict_types=1);

namespace App\Handler;

use App\Car\BaseCar;
use App\Car\CarFactory;
use App\Car\Dto;
use App\Reader\CsvReader;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Throwable;

class CarListHandler
{
    public function __construct(
        private readonly CsvReader $reader,
        private readonly CarFactory $factory,
        private readonly ValidatorInterface $validator,
        private readonly LoggerInterface $logger,
    ) {
    }

    /**
     * @param string $path
     * @return BaseCar[]
     */
    public function __invoke(string $path): array
    {
        $cars = [];
        try {
            $this->reader->open($path);
            /**
             * @var array{
             *     car_type: string,
             *     brand: string,
             *     passenger_seats_count: string,
             *     photo_file_name: string,
             *     body_whl: string,
             *     carrying: string,
             *     extra: string} $line
             */
            foreach ($this->reader->read() as $line) {
                $dto = new Dto(
                    $line['car_type'],
                    $line['brand'],
                    $line['photo_file_name'],
                    $line['carrying'],
                    $line['passenger_seats_count'],
                    $line['body_whl'],
                    $line['extra'],
                );

                if (!$this->validator->validate($dto)->count()) {
                    $cars[] = $this->factory->create($dto);
                }
            }
            $this->reader->close();
        } catch (Throwable $e) {
            $this->logger->error($e->getMessage());
        }

        return $cars;
    }
}