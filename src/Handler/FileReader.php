<?php

declare(strict_types=1);

namespace App\Handler;

interface FileReader
{
    public function read(): iterable;

    public function open(string $path): void;

    public function close(): void;
}