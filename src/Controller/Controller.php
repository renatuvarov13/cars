<?php

declare(strict_types=1);

namespace App\Controller;

use App\Handler\CarListHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class Controller extends AbstractController
{
    #[Route(path: '/', name: 'index', methods: ['POST'])]
    public function index(Request $request, CarListHandler $getCarList, SerializerInterface $serializer): JsonResponse
    {
        /** @var UploadedFile|null $file */
        $file = $request->files->get('cars');

        if (!$file) {
            throw new BadRequestHttpException('Cars is required.');
        }

        $cars = $getCarList($file->getPathname());

        return new JsonResponse(
            $serializer->serialize($cars, 'json'),
            json: true
        );
    }
}