<?php

declare(strict_types=1);

namespace App\Car;

use Exception;

class CarFactory
{
    private const SEPARATOR = 'x';

    private ?Dto $dto = null;

    /**
     * @throws Exception
     */
    public function create(Dto $dto): BaseCar
    {
        $this->dto = $dto;

        $car = match (CarType::tryFrom($dto->carType)) {
            CarType::CAR => $this->createCar(),
            CarType::TRUCK => $this->createTruck(),
            CarType::SPEC_MACHINE => $this->createSpecMachine(),
            default => throw new Exception('Unexpected match value'),
        };

        $this->fillBase($car);

        return $car;
    }

    private function createCar(): Car
    {
        $car = new Car();

        $car->setPassengerSeatsCount((int)$this->dto->passengerSeatsCount);

        return $car;
    }

    private function createTruck(): Truck
    {
        $car = new Truck();

        $whl = explode(self::SEPARATOR, $this->dto->bodyWHL);

        $car->setBodyWidth(isset($whl[0]) ? (float)$whl[0] : 0.0);
        $car->setBodyHeight(isset($whl[1]) ? (float)$whl[1] : 0.0);
        $car->setBodyLength(isset($whl[2]) ? (float)$whl[2] : 0.0);

        return $car;
    }

    private function createSpecMachine(): SpecMachine
    {
        $car = new SpecMachine();

        $car->setExtra($this->dto->extra);

        return $car;
    }

    private function fillBase(BaseCar $car): void
    {
        $car->setCarType(CarType::from($this->dto->carType));
        $car->setBrand($this->dto->brand);
        $car->setPhotoFileName($this->dto->photoFileName);
        $car->setCarrying((float)$this->dto->carrying);
    }
}