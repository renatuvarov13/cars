<?php

declare(strict_types=1);

namespace App\Car;

abstract class BaseCar
{
    private CarType $carType;
    private string $brand;
    private string $photoFileName;
    private float $carrying;

    public function getPhotoFileExt(): ?string
    {
        $ext = pathinfo($this->photoFileName, PATHINFO_EXTENSION);

        return $ext ? ".$ext" : null;
    }

    public function getCarType(): CarType
    {
        return $this->carType;
    }

    public function setCarType(CarType $carType): void
    {
        $this->carType = $carType;
    }

    public function getBrand(): string
    {
        return $this->brand;
    }

    public function setBrand(string $brand): void
    {
        $this->brand = $brand;
    }

    public function getPhotoFileName(): string
    {
        return $this->photoFileName;
    }

    public function setPhotoFileName(string $photoFileName): void
    {
        $this->photoFileName = $photoFileName;
    }

    public function getCarrying(): float
    {
        return $this->carrying;
    }

    public function setCarrying(float $carrying): void
    {
        $this->carrying = $carrying;
    }
}