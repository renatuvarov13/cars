<?php

declare(strict_types=1);

namespace App\Car;

class Truck extends BaseCar
{
    private float $bodyWidth;
    private float $bodyLength;
    private float $bodyHeight;

    public function getBodyVolume(): float
    {
        return $this->bodyWidth * $this->bodyHeight * $this->bodyLength;
    }

    public function getBodyWidth(): float
    {
        return $this->bodyWidth;
    }

    public function setBodyWidth(float $bodyWidth): void
    {
        $this->bodyWidth = $bodyWidth;
    }

    public function getBodyLength(): float
    {
        return $this->bodyLength;
    }

    public function setBodyLength(float $bodyLength): void
    {
        $this->bodyLength = $bodyLength;
    }

    public function getBodyHeight(): float
    {
        return $this->bodyHeight;
    }

    public function setBodyHeight(float $bodyHeight): void
    {
        $this->bodyHeight = $bodyHeight;
    }
}