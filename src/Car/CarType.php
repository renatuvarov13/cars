<?php

declare(strict_types=1);

namespace App\Car;

enum CarType: string
{
    case CAR = 'car';
    case TRUCK = 'truck';
    case SPEC_MACHINE = 'spec_machine';

    public static function values(): array
    {
        return array_column(self::cases(), 'value');
    }
}
