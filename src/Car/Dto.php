<?php

declare(strict_types=1);

namespace App\Car;

use Symfony\Component\Validator\Constraints as Assert;

class Dto
{
    public function __construct(
        #[Assert\Choice(
            callback: [CarType::class, 'values'],
            message: 'Unexpected car type'
        )]
        public readonly string $carType,

        #[Assert\NotBlank]
        public readonly string $brand,

        #[Assert\NotBlank]
        public readonly string $photoFileName,

        #[Assert\NotBlank]
        public readonly string $carrying,

        #[Assert\Expression(
            expression: 'this.carType in ["car"] or !this.passengerSeatsCount',
            message: '$passengerSeatsCount should not be present for this type of car'
        )]
        public readonly string $passengerSeatsCount,

        #[Assert\Expression(
            expression: 'this.carType in ["truck"] or !this.bodyWHL',
            message: '$bodyWHL should not be present for this type of car'
        )]
        public readonly string $bodyWHL,

        #[Assert\Expression(
            expression: 'this.carType in ["spec_machine"] or !this.extra',
            message: '$extra should not be present for this type of car'
        )]
        public readonly string $extra,
    ) {
    }
}