<?php

declare(strict_types=1);

namespace App\Reader;

use App\Handler\FileReader;

class CsvReader implements FileReader
{
    private const SEPARATOR = ';';

    /**
     * @var resource
     */
    private $resource;

    public function read(): iterable
    {
        $firstRow = true;
        $header = [];

        while (($line = $this->readLine()) !== false) {

            if ($firstRow) {
                $header = $line;
                $firstRow = false;
            } elseif (count($header) === count($line)) {
                yield array_combine($header, $line);
            }
        }
    }

    private function readLine(): array|false
    {
        return fgetcsv($this->resource, 0, self::SEPARATOR);
    }

    public function open(string $path): void
    {
        $this->resource = fopen($path, 'r');
    }

    public function close(): void
    {
        if ($this->resource) {
            fclose($this->resource);
        }
    }
}