init: up install

up:
	docker-compose up -d --build

down:
	docker-compose down

install:
	docker exec -it app-php composer i

bash:
	docker exec -it app-php bash

test:
	docker exec -it app-php composer test